var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// Set locals
	locals.section = 'bookReview';
	locals.filters = {
		post: req.params.book
	};

	locals.data = {
	};
	
	// Load the current post
	view.on('init', function(next) {

		var q = keystone.list('BookReview').model.findOne({
			slug: locals.filters.post
		});
		
		q.exec(function(err, result) {
			locals.data.post = result;
			next(err);
		});
		
	});
	
	// Render the view
	view.render('bookReview');
	
};
