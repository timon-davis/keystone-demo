var keystone = require( 'keystone' ),
	Types = keystone.Field.Types;

var BookReview = new keystone.List( 'BookReview', {
	map: { name: 'bookTitle' },
	autokey: { path: 'slug', from: 'bookTitle', unique: true }
} );

BookReview.add({

	bookTitle: { type: Types.Text, required: true },
	author: { type: Types.Text },
	review: { type: Types.Html, wysiwyg: true },
	publishDate: { type: Types.Date }

});

BookReview.defaultColumns = "bookTitle|50%, author|50%";
BookReview.register();